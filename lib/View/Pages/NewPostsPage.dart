// ignore_for_file: prefer_const_constructors, camel_case_types, prefer_const_constructors_in_immutables, unnecessary_import, file_names, unused_field, non_constant_identifier_names, avoid_print

import 'dart:ui';

import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/PostModel.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/value/images.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:app1/View/widgets/alert.dart';
import 'package:app1/View/widgets/bottombar.dart';
import 'package:app1/View/widgets/getTextFormField.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

class NewPosts extends StatefulWidget {
  NewPosts({ Key? key}) : super(key: key);

  @override
  State<NewPosts> createState() => _NewPostsState();
}

class _NewPostsState extends State<NewPosts> {
  final titleController = TextEditingController();
  final bodyController = TextEditingController();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  final formkey = GlobalKey<FormState>();
  String? name;
  late String user_id;
  late String user_name;
  late String user_email;
  late String user_gender;
  late String user_status;

  @override
  void initState() {
    super.initState();
    // _conStatus.text;
    getData();
  }

  Future getData() async {
    final SharedPreferences sp = await pref;
    user_id = sp.getString('user_id') ?? "";
    print(user_id);
    user_name = sp.getString('user_name') ?? "";
    user_email = sp.getString('user_email') ?? "";
    user_gender = sp.getString('user_gender') ?? "";
    user_status = sp.getString('user_status') ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Form(
        key: formkey,
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  GetTextFormField(
                    controller: titleController,
                    color: Colors.grey.shade200.withOpacity(0.5),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  GetTextFormField(
                    controller: bodyController,
                    color: Colors.grey.shade200.withOpacity(0.5),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (titleController.text.isEmpty ||
                          bodyController.text.isEmpty) {
                        alertDialog('nhap day du tittle va body');
                      } else {
                        Post newposts = Post(
                            userId: int.parse(user_id),
                            title: titleController.text,
                            body: bodyController.text);
                        await ApiService()
                            .createPosts(newposts, context)
                            .then((data) {
                          if (data == null) {
                            alertDialog("Successful");
                            Future.delayed(Duration(seconds: 1)).then(
                              (value) => Navigator.push(
                                context,
                                CustomPageRoute(
                                    child: BottomBar(
                                    ),
                                    direction: AxisDirection.left),
                                //  (route) => false)
                              ),
                            );
                          }
                        });
                      }
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Upload',
                            style: AppStyles.h3.copyWith(
                                fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  // ElevatedButton(
                  //     child: Text('Upload'),
                  //     onPressed: () async {
                  //       Post newposts = Post(
                  //           userId: user_id,
                  //           title: titleController.text,
                  //           body: bodyController.text);
                  //       await PostData().createPosts(newposts);
                  //     }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    // SingleChildScrollView(
    //   child: Padding(
    //     padding: EdgeInsets.all(8.0),
    //     child: Column(
    //       children: [
    //         Padding(
    //           padding: EdgeInsets.all(8.0),
    //           child: TextField(
    //             minLines: null,
    //             maxLines: null,
    //             decoration: InputDecoration(
    //               labelText: 'Nhap tieu de',
    //               fillColor: Colors.black12,
    //               filled: false,
    //               border: OutlineInputBorder(
    //                   borderRadius: BorderRadius.all(Radius.circular(8.0))),
    //             ),
    //             controller: titleController,
    //           ),
    //         ),
    //         Padding(
    //           padding: EdgeInsets.all(8.0),
    //           child: TextField(
    //             // minLines: null,
    //             // maxLines: null,
    //             decoration: InputDecoration(
    //               labelText: 'Nhap body',
    //               fillColor: Colors.black12,
    //               filled: false,
    //               border: OutlineInputBorder(
    //                   borderRadius: BorderRadius.all(Radius.circular(8.0))),
    //             ),
    //             controller: bodyController,
    //           ),
    //         ),
    //         ElevatedButton(
    //             child: Text('Upload'),
    //             onPressed: () async {
    //               Post newposts = Post(
    //                   userId: defuserId,
    //                   title: titleController.text,
    //                   body: bodyController.text);
    //               await PostData().createPosts(newposts);
    //             }),
    //       ],
    //     ),
    //   ),
    // ),
  }
}
