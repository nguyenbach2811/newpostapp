// ignore_for_file: camel_case_types

import 'dart:ui';

import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/CommentModel.dart';
import 'package:app1/Model/PostModel.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/value/images.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:app1/View/widgets/alert.dart';
import 'package:app1/View/widgets/getTextFormField.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'CommentPage.dart';

class createComs extends StatefulWidget {
  int? postId;
  Post post1;
  // createComs(int? postId, {Key? key}) : super(key: key) {
  //   this.postId = postId;
  // }

  createComs(this.post1, {Key? key}) : super(key: key);
  @override
  State<createComs> createState() => _createComsState();
}

class _createComsState extends State<createComs> {
  ConnectivityResult result = ConnectivityResult.none;
  final comController = TextEditingController();
  final formkey = GlobalKey<FormState>();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late String user_id;
  late String user_name;
  late String user_email;
  late String user_gender;
  late String user_status;
  // late String post_id;
  // late String comment_userName;
  // late String comment_body;
  // late String comment_email;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }
  Future getData() async {
    final SharedPreferences sp = await pref;
    user_id = sp.getString('user_id') ?? "";
    user_name = sp.getString('user_name') ?? "";
    user_email = sp.getString('user_email') ?? "";
    user_gender = sp.getString('user_gender') ?? "";
    user_status = sp.getString('user_status') ?? "";
  }

  Future setData(Comment comment) async {
    final SharedPreferences sp = await pref;
    sp.setString('post_id', comment.postId.toString());
    sp.setString('comment_name', comment.name);
    sp.setString('comment_body', comment.body);
    sp.setString('comment_email',comment.email);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('new coms')),
      body: Form(
        key: formkey,
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  GetTextFormField(
                    controller: comController,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () async {
                      if (comController.text.isEmpty) {
                        alertDialog('nhap cmt');
                      } else {
                        Comment newcom = Comment(
                          postId: widget.post1.id,
                          name: user_name,
                          body: comController.text,
                          email: user_email,
                        );
                        await ApiService().createComs(newcom).then((data) {
                          // print(data);
                          if (data == null) {
                            // print(data);
                            alertDialog("Successful");
                            Future.delayed(const Duration(seconds: 1)).then(
                                (value) =>
                                    setData(newcom).whenComplete(() {
                                      Navigator.pushAndRemoveUntil(context, CustomPageRoute(child: CommentPage(),direction: AxisDirection.right), (route) => false);
                                    }));
                          }
                        }

                            // if (check == 1) {
                            //   showDialog(
                            //       context: context,
                            //       builder: (context) => AlertDialog(
                            //             title: Text("thong bao"),
                            //             content: Text("upload thanh cong"),
                            //             actions: [
                            //               TextButton(
                            //                   onPressed: (() {
                            //                     Navigator.pop(
                            //                         context,
                            //                         MaterialPageRoute(
                            //                           builder: (context) =>
                            //                               HomePage(
                            //                                   usermodel: widget
                            //                                       .userModel),
                            //                         ));
                            //                   }),
                            //                   child: Text("ok"))
                            //             ],
                            //           ));
                            // }
                            );
                      }
                      // check = 0;
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Upload',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  // ElevatedButton(
                  //     child: Text('Upload'),
                  //     onPressed: () async {
                  //       Post newposts = Post(
                  //           userId: user_id,
                  //           title: titleController.text,
                  //           body: bodyController.text);
                  //       await PostData().createPosts(newposts);
                  //     }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//   check() async {
//     result = await Connectivity().checkConnectivity();
//     if (result == ConnectivityResult.mobile ||
//         result == ConnectivityResult.wifi) {
//       checkva();
//     } else if (result == ConnectivityResult.none) {
//       return showDialog(
//           context: context,
//           barrierDismissible: false,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: const Text('Please check internet connection'),
//               actions: <Widget>[
//                 Center(
//                   child: TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: const Text('Ok'),
//                   ),
//                 ),
//               ],
//             );
//           });
//     }
//   }
// }
}
