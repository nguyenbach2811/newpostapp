import 'package:app1/Api/API_Constants.dart';
import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/Pages/RegisterPage.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:app1/View/widgets/alert.dart';
import 'package:app1/View/widgets/getTextField.dart';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../widgets/bottombar.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _conUserName = TextEditingController();
  final _conEmail = TextEditingController();
  final formkey = GlobalKey<FormState>();
  String token2 = APIConstans.token;
  final storage = const FlutterSecureStorage();
  List<UserModel> users = [];
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  ConnectivityResult result = ConnectivityResult.none;
  bool isApiCallprocess = false;

  @override
  void initState() {
    super.initState();
  }

  login() async{
    String email1 = _conEmail.text;
    String name1 = _conUserName.text;
    bool check = false;
    final bool isValidEmail = EmailValidator.validate(email1);
    if (formkey.currentState!.validate()) {
      if (name1.isEmpty) {
        alertDialog("please input email!");
      } else if (email1.isEmpty) {
        alertDialog("please input name");
      } else if (isValidEmail == false) {
        alertDialog("Invalid Email");
      } else {
        formkey.currentState!.save();
        for (int i = 0; i < users.length; i++) {
          if (users[i].email!.contains(email1) &&
              users[i].name!.contains(name1)) {
            check = true;
            alertDialog("Complete");
            await storage.write(key: 'token', value: users[i].toString());
            setSP(users[i]).whenComplete(() {
              Navigator.pushAndRemoveUntil(
                  context,
                  CustomPageRoute(
                      child: BottomBar(
                      ),
                      direction: AxisDirection.right),
                      (route) => false);
            });

          }
        }
        if (check == false) {
          alertDialog("User not found!!");
        }
      }
    }
  }

  check() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      login();
    } else if (result == ConnectivityResult.none) {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  Future setSP(UserModel userModel) async{
    final SharedPreferences sp = await pref;
    sp.setString('usermodel', userModel.toString());
    sp.setString('user_id', userModel.id.toString());
    sp.setString('user_name', userModel.name ?? "");
    sp.setString('user_email', userModel.email ?? "");
    sp.setString('user_gender', userModel.gender ?? "");
    sp.setString('user_status', userModel.status ?? "");
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
            Colors.purple,
            Colors.blue,
            Color.fromARGB(255, 53, 38, 163)
          ])),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        extendBody: true,
        body: Form(
          key: formkey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                SizedBox(
                  height: size.height * 1 / 5,
                ),
                Stack(
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: size.height * 1 / 7,
                          width: size.width * 1 / 2,
                          child: FutureBuilder(
                              future: ApiService().getAllUser(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<List<UserModel>> snapshot) {
                                if (snapshot.hasData) {
                                  users = snapshot.data!;
                                  return Container();
                                }
                                return Center(
                                    child: Column(
                                  children: [
                                    const CircularProgressIndicator(),
                                    SizedBox(
                                      height: size.height* 1/20,
                                    ),
                                    Text(
                                      'Loading data .........',
                                      style: AppStyles.h3.copyWith(
                                          color: Colors.white, fontSize: 20),
                                    )
                                  ],
                                ));
                              }),
                        ),
                        GetTextField(
                          controller: _conUserName,
                          hintName: 'Name',
                          color: Colors.transparent,
                          icon: Icons.person,
                        ),
                        GetTextField(
                          controller: _conEmail,
                          hintName: 'Email',
                          color: Colors.transparent,
                          icon: Icons.email,
                        ),
                        SizedBox(
                          height: size.height * 1 / 15,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isApiCallprocess = true;
                            });
                            check();
                          },
                          child: Container(
                            height: size.height*1/15,
                            width: size.width*1/2,
                            decoration: BoxDecoration(
                              gradient: const LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Color.fromARGB(255, 243, 247, 246),
                                  Color.fromARGB(255, 239, 244, 244),
                                ],
                              ),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Center(
                              child: Text('SIGN IN',
                                  style: AppStyles.h3.copyWith(
                                    fontSize: 25,
                                    color: Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.height * 1/20,
                        ),
                        Center(
                          child: TextButton(
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    CustomPageRoute(
                                        child: const RegisterPage(),
                                        direction: AxisDirection.right),
                                    (route) => false);
                              },
                              child: Text("Create your account",
                                  style: AppStyles.h3.copyWith(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  ))),
                        ),
                        SizedBox(
                          height: size.height * 1 / 300,
                        ),
                        Text(
                          '_________Or_________',
                          style: AppStyles.h3
                              .copyWith(color: Colors.white, fontSize: 20),
                        ),
                        SizedBox(
                          height: size.height * 1 / 40,
                        ),
                        Center(
                          child: Text(
                            'Login with Social Media',
                            style: AppStyles.h3.copyWith(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: size.width * 1 / 3.3,
                            ),
                            IconButton(
                              icon: const Icon(Icons.facebook),
                              color: Colors.white,
                              iconSize: 40,
                              onPressed: () {},
                            ),
                            IconButton(
                              icon: const Icon(FontAwesomeIcons.twitter),
                              color: Colors.white,
                              iconSize: 40,
                              onPressed: () {},
                            ),
                            IconButton(
                              icon: const Icon(FontAwesomeIcons.instagram),
                              color: Colors.white,
                              iconSize: 40,
                              onPressed: () {},
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
