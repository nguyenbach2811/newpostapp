import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/CommentModel.dart';
import 'package:app1/Model/PostModel.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/value/icons.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/alert.dart';
import 'package:app1/View/widgets/getTextFormField.dart';
import 'package:app1/View/widgets/getTextFormField2.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CommentPage extends StatefulWidget {
  bool? check;
  @override
  State<CommentPage> createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  List<Comment>? comments;
  var isload2 = false;
  final comController = TextEditingController();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late String post_id;
  late String post_title;
  late String user_id;
  late String user_name;
  late String user_email;


  @override
  void initState() {
    super.initState();
    getData();
    getData2();
    setState(() {

    });
  }

  getData() async {
    final SharedPreferences sp = await pref;
    post_id = sp.getString('post_id') ?? "";
    post_title = sp.getString('post_title') ?? "";
      comments = await ApiService().getCom(post_id);
      if (comments != null) {
        isload2 = true;
      }
      setState(() {

      });
  }
  getData2() async {
    final SharedPreferences sp1 = await pref;
    user_id = sp1.getString('user_id') ?? "";
    user_name = sp1.getString('user_name') ?? "";
    user_email = sp1.getString('user_email') ?? "";
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(isload2.toString());
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text(post_title),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Visibility(
              visible: isload2,
              child: SizedBox(
                height: h*7.8/10,
                width: w,
                child: ListView.builder(
                  itemCount: comments?.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.grey[300]),
                          ),
                          SizedBox(width: 16),

                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  comments![index].email,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  comments![index].body,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
            Row(
              children: [
                SizedBox(width: w*1/18,),
                GetTextFormField2(
                  controller: comController,
                  color: Colors.grey[200],
                ),
                SizedBox(width: w*1/20,),
                InkWell(
                    child: Icon(AppIcons.send, size: 35, color: Colors.blue,),
                    onTap: () async {
                      if (comController.text.isEmpty)
                        alertDialog('nhap cmt');
                      else {
                        Comment newcom = Comment(
                          postId: int.parse(post_id),
                          name: user_name,
                          body: comController.text,
                          email: user_email,
                        );
                        await ApiService().createComs(newcom).then((data) {
                          // print(data);
                          if (data == null) {
                            // print(data);
                            alertDialog("Successful");
                            comments?.insert(0, newcom);
                            setState(() {});
                            // getData2();
                            comController.text = "";
                          }
                          ;
                        });
                      }
                    }),
              ],
            ),


          ],
        ),
      ),

    );
  }
}
