import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/Pages/StartedPage.dart';
import 'package:app1/View/widgets/alert.dart';
import 'package:app1/View/widgets/getTextFormField.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';



import '../value/styles.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _conEmail = TextEditingController();
  final _conName = TextEditingController();
  final _conGender = TextEditingController();
  final _conStatus = TextEditingController();
  final formkey = GlobalKey<FormState>();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late String user_id;
  late String user_name;
  late String user_email;
  late String user_gender;
  late String user_status;

  // bool hasInternet = false;
  ConnectivityResult result = ConnectivityResult.none;
  final storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    getData();
  }

  update() async {
    String email = _conEmail.text;
    String name = _conName.text;
    String gender1 = _conGender.text;
    String status1 = _conStatus.text;
    if (formkey.currentState!.validate()) {
      if (gender1 != 'male' && gender1 != 'female') {
        alertDialog('wrong gender!');
      } else if (status1 != 'active' && status1 != 'inactive') {
        alertDialog("Wrong status!");
      } else {
        formkey.currentState!.save();
        UserModel user1 = UserModel(
            email: email, name: name, gender: gender1, status: status1);

        await ApiService()
            .updateUser(user_id, user1)
            .then((data) {
          setState(() {
            alertDialog("Successful Update!");
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => const StartedPage()),
                (route) => false);
          });
        }).catchError((error) {
          print(error);
          alertDialog("Error");
        });
      }
    }
  }

  getData() async {
    final SharedPreferences sp = await pref;
    user_id = sp.getString('user_id') ?? "";
    user_name = sp.getString('user_name') ?? "";
    user_email = sp.getString('user_email') ?? "";
    user_gender = sp.getString('user_gender') ?? "";
    user_status = sp.getString('user_status') ?? "";
    setState(() {
      _conName.text = user_name;
      _conEmail.text = user_email;
      _conGender.text = user_gender;
      _conStatus.text = user_status;
    });
  }

  check() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      update();
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  delete() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      await storage.delete(key: 'token');
      setState(() {
        ApiService().deleteUser(user_id);
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const StartedPage()),
            (route) => false);
      });
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  logOut() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      await storage.delete(key: 'token');
      setState(() {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const StartedPage()),
            (route) => false);
      });
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      decoration: const BoxDecoration(color: Color.fromARGB(255, 29, 29, 29)),
      child: Scaffold(
        // extendBody: true,
        backgroundColor: Colors.transparent,
        body: Form(
            key: formkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: size.height * 1 / 10,
                  ),
                  GetTextFormField(
                    controller: _conName,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conEmail,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conGender,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conStatus,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () async {
                      check();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Update',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 1 / 25,
                  ),
                  GestureDetector(
                    onTap: () {
                      delete();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Delete',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 1 / 25,
                  ),
                  GestureDetector(
                    onTap: () {
                      logOut();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Log out',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
