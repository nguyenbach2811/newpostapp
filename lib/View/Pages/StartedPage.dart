
import 'package:app1/View/Pages/LoginPage.dart';
import 'package:app1/View/Pages/RegisterPage.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class StartedPage extends StatefulWidget {
  const StartedPage({Key? key}) : super(key: key);

  @override
  State<StartedPage> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<StartedPage> {
  ConnectivityResult result = ConnectivityResult.none;
  bool checkInternet = false;

  @override
  void initState() {
    super.initState();
  }

  check() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.wifi ||
        result == ConnectivityResult.mobile) {
      Navigator.of(context).pushAndRemoveUntil(
          CustomPageRoute(
              child: const LoginPage(), direction: AxisDirection.left),
          (route) => false);
    } else if (result == ConnectivityResult.none) {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'ok',
                      style: AppStyles.h3
                          .copyWith(color: Colors.blue, fontSize: 22),
                    ),
                  ),
                ),
              ],
            );
          });
    }
  }

  signUp() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.wifi ||
        result == ConnectivityResult.mobile) {
      Navigator.pushAndRemoveUntil(
          context,
          CustomPageRoute(child: const RegisterPage(), direction: AxisDirection.left),
          (route) => false);
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
            Colors.purple,
            Colors.blue,
            Color.fromARGB(255, 53, 38, 163)
          ])),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              SizedBox(
                height: h * 1 / 5.5,
              ),
              Text(
                "WELCOME TO MY APP",
                style: AppStyles.h3.copyWith(
                    fontSize: 35,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              SizedBox(
                height: w*1/50,
              ),
              Container(
                width: w * 1 / 1.2,
                // padding: const EdgeInsets.only(top: 20),
                child: Text(
                  "Let's connect with people all over the world",
                  style: AppStyles.h3.copyWith(fontSize: 20),
                ),
              ),
              SizedBox(
                height: h * 1 / 6,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    check();
                  },
                  child: Container(
                    height: h*1/15,
                    width: w * 1/2,
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color.fromARGB(255, 243, 247, 246),
                          Color.fromARGB(255, 239, 244, 244),
                        ],
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Center(
                      child: Text('SIGN IN',
                          style: AppStyles.h3
                              .copyWith(fontSize: 25, color: Colors.black)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: h * 1 / 25,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    signUp();
                  },
                  child: Container(
                    height: h*1/15,
                    width: w * 1/2,
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                          Color.fromARGB(255, 243, 247, 246),
                          Color.fromARGB(255, 239, 244, 244),
                        ],
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Center(
                      child: Text('SIGN UP',
                          style: AppStyles.h3
                              .copyWith(fontSize: 25, color: Colors.black)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: h * 1 / 4.5,
              ),
              Center(
                child: Text(
                  'Login with Social Media',
                  style: AppStyles.h3.copyWith(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    width: w * 1 / 3.6,
                  ),
                  IconButton(
                    icon: const Icon(Icons.facebook),
                    color: Colors.white,
                    iconSize: 40,
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: const Icon(FontAwesomeIcons.twitter),
                    color: Colors.white,
                    iconSize: 40,
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: const Icon(FontAwesomeIcons.instagram),
                    color: Colors.white,
                    iconSize: 40,
                    onPressed: () {},
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
