
import 'package:app1/Api/Api_Service.dart';
import 'package:app1/Model/PostModel.dart';
import 'package:app1/View/Pages/CommentPage.dart';
import 'package:app1/View/value/styles.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:auto_route/auto_route.dart';


class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  Demo createState() => Demo();
}

class Demo extends State<HomePage> {
  List<Post>? posts;
  var isLoaded = false;
  final storage = const FlutterSecureStorage();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late String user_id;
  late String user_name;
  late String user_email;
  late String user_gender;
  late String user_status;


  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    getData();
  }
  Future getData() async {
    posts = await ApiService().getPosts();
    setState(
      () {
        isLoaded = true;
      },
    );
    final SharedPreferences sp = await pref;
    user_id = sp.getString('user_id') ?? "";
    user_name = sp.getString('user_name') ?? "";
    user_email = sp.getString('user_email') ?? "";
    user_gender = sp.getString('user_gender') ?? "";
    user_status = sp.getString('user_status') ?? "";
  }
  Future setData(Post post1) async {
    final SharedPreferences sp = await pref;
    setState(() {
      sp.setString('post_id', post1.id.toString());
      sp.setString('post_title', post1.title);
      sp.setString('post_body', post1.body);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: build_AppBar(context),
      backgroundColor: Color.fromARGB(255, 9, 9, 9),
      body:
      FutureBuilder(
        future: ApiService().getPosts(),
        builder: (BuildContext context, AsyncSnapshot<List<Post>?> snapshot) {
          if (snapshot.hasData) {
            List<Post>? posts = snapshot.data;
            return ListView(
              children: posts!
                  .map(
                    (Post post) =>
                    Card(
                      color: Color.fromARGB(255, 33, 33, 33),
                      margin: const EdgeInsets.all(8.0),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ListTile(
                          title: Text(
                            post.title,
                            style: AppStyles.h3.copyWith(fontSize: 22, color: Colors.white)
                          ),
                          subtitle:
                              Padding(
                                padding: const EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                    post.body,
                                    style: AppStyles.h3.copyWith(fontSize: 16, color: Colors.white)
                                ),
                              ),
                          onTap: () {
                            setData(post).whenComplete(() {

                              Navigator.of(context).push(CustomPageRoute(child: CommentPage(),direction: AxisDirection.left));
                            });
                          },
                        ),
                      ),
                    ),
                  )
                  .toList(),
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ));
    }
}

