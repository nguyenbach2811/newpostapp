import 'package:app1/View/value/styles.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class GetTextField extends StatelessWidget {
  TextEditingController? controller;
  String? hintName;
  IconData? icon;
  bool isObscureText = false;
  TextInputType inputType;
  bool _passwordVisible = false;
  Color? color;

  GetTextField(
      {this.controller,
      this.hintName,
      this.icon,
      this.color,
      this.inputType = TextInputType.text,
      this.isObscureText = false});
  @override
  void initState() {
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Container(
      padding: const EdgeInsets.only(top: 25, left: 50, right: 50),
      child: TextField(
        style: AppStyles.h3.copyWith(fontSize: 18, color: Colors.white),
        controller: controller,
        obscureText: isObscureText,
        keyboardType: inputType,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white)
            ),
            focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Colors.white)),
            prefixIcon: Icon(icon, color: Color.fromARGB(255, 215, 207, 207)),
            hintText: hintName,
            fillColor: color,
            filled: true,
            hintStyle:
                AppStyles.h3.copyWith(fontSize: 15, color: Colors.white)),
      ),
    );
  }
}
