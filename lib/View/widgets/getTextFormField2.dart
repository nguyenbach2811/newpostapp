import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class GetTextFormField2 extends StatelessWidget {
  TextEditingController? controller;
  String? hintName;
  String? labelName;
  IconData? icon;
  bool isObscureText = false;
  TextInputType inputType;
  Color? color;

  GetTextFormField2(
      {super.key, this.controller,
        this.hintName,
        this.labelName,
        this.icon,
        this.color,
        this.inputType = TextInputType.text,
        this.isObscureText = false});
  void initState() {
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return SizedBox(
      width: w * 3/4,
      child: TextFormField(
        controller: controller,
        obscureText: isObscureText,
        keyboardType: inputType,

        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter $hintName';
          }
          return null;
        },
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: const BorderSide(color: Colors.transparent)),
            focusedBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: const BorderSide(color: Colors.white)),
            errorStyle: const TextStyle(color: Colors.red),
            focusedErrorBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: const BorderSide(color: Colors.white)),
            errorBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: const BorderSide(color: Colors.red)),
            prefixIcon: Icon(icon),
            labelText: labelName,
            hintText: hintName,
            // contentPadding: const EdgeInsets.only(left: 20),
            fillColor: color,
            filled: true),
      ),
    );
  }
}
