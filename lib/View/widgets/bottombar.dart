// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/Pages/CommentPage.dart';
import 'package:app1/View/Pages/HomePage.dart';
import 'package:app1/View/Pages/LoadingPage.dart';
import 'package:app1/View/Pages/NewComPage.dart';
import 'package:app1/View/Pages/ProfilePage.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:app1/View/widgets/tab_navigator.dart';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Pages/StartedPage.dart';
import '../Pages/NewPostsPage.dart';
import 'package:flutter/cupertino.dart';

class BottomBar extends StatefulWidget {
  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int currentIndex = 0;
  int? tappedIndex;
  late UserModel user;
  late var Screens;
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  late String user_id;
  late String user_name;
  late String user_email;
  late String user_gender;
  late String user_status;
  final _pageController = PageController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  Future getData() async {
    final SharedPreferences sp = await pref;
      user_id = sp.getString('user_id') ?? "";
      user_name = sp.getString('user_name') ?? "";
      user_email = sp.getString('user_email') ?? "";
      user_gender = sp.getString('user_gender') ?? "";
      user_status = sp.getString('user_status') ?? "";
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        extendBody: true,
        backgroundColor: Color.fromARGB(255, 6, 35, 59),
        body: Stack(
          children: <Widget>[
            PageTransitionSwitcher(
              transitionBuilder: (child, primaryAnimation, secondaryAnimation) =>
                  FadeThroughTransition(
                    animation: primaryAnimation,
                    secondaryAnimation: secondaryAnimation,
                    child: child,
                  ),
              child: PageView(
                controller: _pageController,
                children: [
                  HomePage(),
                  NewPosts(),
                  ProfilePage(),
                ],
                onPageChanged: (index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
              ),
            ),
          ],

        ),
        bottomNavigationBar: Container(
          child: Theme(
            data: Theme.of(context).copyWith(
              canvasColor:
                  const Color.fromARGB(255, 40, 40, 42).withOpacity(0.8),
            ),
            child: Container(
              height: size.height * 1 / 11,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 5,
                      sigmaY: 10,
                    ),
                    child: BottomNavigationBar(
                      type: BottomNavigationBarType.fixed,
                      selectedItemColor: const Color.fromARGB(255, 60, 122, 218),
                      unselectedItemColor: Colors.grey,
                      // iconSize: 30,
                      // selectedFontSize: 18,
                      // unselectedFontSize: 15,
                      items: const <BottomNavigationBarItem>[
                        BottomNavigationBarItem(
                          icon: Icon(Icons.home),
                          label: 'Home',
                        ),
                        BottomNavigationBarItem(
                            icon: Icon(Icons.add), label: 'Add'),
                        BottomNavigationBarItem(
                            icon: Icon(Icons.person), label: 'Profile'),
                      ],
                      currentIndex: currentIndex,
                      onTap: (index) {
                        _pageController.jumpToPage(index);
                        setState(() {
                          currentIndex = index;
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
