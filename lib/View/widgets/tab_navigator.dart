import 'package:app1/View/Pages/HomePage.dart';
import 'package:app1/View/Pages/NewPostsPage.dart';
import 'package:app1/View/Pages/ProfilePage.dart';
import 'package:app1/View/widgets/CustomPageRoute.dart';
import 'package:flutter/material.dart';

class TabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState>? navigatorKey;
  final String? tabItem;

  const TabNavigator({Key? key, this.navigatorKey, this.tabItem}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    late Widget child;
    if (tabItem == "Page1"){
      child = const HomePage();
    }
    else if (tabItem == "Page2"){
      child = NewPosts();
    }
    else if ( tabItem == "page3"){
      child = ProfilePage();
    }
    return Navigator(
      key: navigatorKey,
      onGenerateRoute: (routeSettings) {
        return CustomPageRoute(child: child,direction: AxisDirection.left);
      },
    );
  }
}
