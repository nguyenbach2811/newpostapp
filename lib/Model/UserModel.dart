// ignore_for_file: file_names

class UserModel {
  int? id;
  String? name;
  String? email;
  String? gender;
  String? status;
  String access_Token = 'Access_token';

  UserModel({this.id, this.email, this.name, this.gender, this.status,access_Token});

  // factory UserModel.fromJson(Map<String, dynamic> json) {
  //   return UserModel(
  //     id: json['id'] ?? "".toString(),
  //     name: json['name'] ?? "",
  //     email: json['email'] ?? "",
  //     gender: json['gender'] ?? "",
  //     status: json['status'] ?? "",
  //   );
  // }
  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'] ?? "".toString(),
      name: json['name'] ?? "",
      email: json['email'] ?? "",
      gender: json['gender'] ?? "",
      status: json['status'] ?? "",
      access_Token: json['access_token'] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      // 'id': id.toString(),
      'name': name,
      'email': email,
      'gender': gender,
      'status': status
    };
    return map;
  }
}
