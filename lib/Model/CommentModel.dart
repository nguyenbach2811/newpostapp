// To parse this JSON data, do
//
//     final comment = commentFromJson(jsonString);

// ignore_for_file: file_names

import 'dart:convert';

List<Comment> commentFromJson(String str) =>
    List<Comment>.from(json.decode(str).map((x) => Comment.fromJson(x)));

String commentToJson(List<Comment> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Comment {
  Comment({
    this.id,
    this.postId,
    required this.name,
    required this.email,
    required this.body,
  });

  int? id;
  int? postId;
  String name;
  String email;
  String body;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json["id"],
        postId: json["post_id"],
        name: json["name"],
        email: json["email"],
        body: json["body"],
      );

  Map<String, dynamic> toJson() => {
        // "id": id,
        // "post_id": postId,
        "name": name,
        "email": email,
        "body": body,
      };
}
