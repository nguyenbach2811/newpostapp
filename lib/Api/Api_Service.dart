// ignore_for_file: file_names

import 'dart:convert';

import 'package:app1/Api/API_Constants.dart';
import 'package:app1/Model/CommentModel.dart';
import 'package:app1/Model/PostModel.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class ApiService with ChangeNotifier {
  String url_user = APIConstans.url_User;
  String url_post = APIConstans.url_Post;
  String url_comment = APIConstans.url_Comment;
  String token1 = APIConstans.token;
  final storage = const FlutterSecureStorage();


  Future<List<UserModel>> getAllUser() async {
    http.Response res = await http.get(
      Uri.parse(APIConstans.url_User),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1',
      },
    );
    if (res.statusCode == 200) {
      List response = json.decode(res.body);
      List<UserModel> list =
          response.map((dynamic item) => UserModel.fromJson(item)).toList();
      return list;
    } else {
      throw "Can't get Users";
    }
  }

  Future<UserModel> getUser(int id) async {
    http.Response res = await http.get(
      Uri.parse('$url_user/$id'),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1',
      },
    );
    if (res.statusCode == 200) {
      Map<String, dynamic> user = jsonDecode(res.body);
      return UserModel.fromJson(jsonDecode(res.body));

    } else {
      throw res.statusCode;
    }
  }

  Future<List<UserModel>> getUserByName(String? name) async {
    http.Response res = await http.get(
      Uri.parse("https://gorest.co.in/public/v2/users?name=$name"),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1',
      },
    );
    if (res.statusCode == 200) {
      List response = json.decode(res.body);
      List<UserModel> list =
          response.map((dynamic item) => UserModel.fromJson(item)).toList();
      return list;
    } else {
      throw "Can't get posts.";
    }
  }

  Future<UserModel?> updateUser(String id, UserModel userModel1) async {
    Map data = {
      'name': userModel1.name,
      'email': userModel1.email,
      'gender': userModel1.gender,
      'status': userModel1.status,
    };
    http.Response res = await http.put(Uri.parse("$url_user/$id"),
        headers: <String, String>{
          'Authorization':
              'Bearer $token1',
        },
        body: data
        // 'id': userModel.id,

        );
    if (res.statusCode == 200) {
      print(res.body);
      return UserModel.fromJson(jsonDecode(res.body));
    } else {
      print(res.statusCode);
    }
    return null;
  }

  Future<UserModel?> deleteUser(String id) async {
    final http.Response response = await http.delete(
      Uri.parse('$url_user/$id'),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1',
      },
    );
    if (response.statusCode == 200) {
      return UserModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
    }
    return null;
  }

  Future<UserModel?> register(UserModel user) async {
    final http.Response response = await http.post(
      Uri.parse(url_user),
      body: user.toJson(),
      headers: <String, String>{
        // 'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':
            'Bearer $token1',
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      return UserModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
    }
  }

  Future<List<Comment>?> getCom(String? id) async {
    var client = http.Client();
    var uri1 = Uri.parse('$url_post$id/comments');
    var response1 = await client.get(
      uri1,
      headers: <String, String>{
        'Authorization':
            'Bearer $token1'
      },
    );
    var json = response1.body;
    if (response1.statusCode == 200) {
      // List<Comment>? data = commentFromJson(json)
      //     .where((element) => element.postId == id)
      //     .toList();
      // return data;
      return commentFromJson(json);
    }
    // commentFromJson(json).clear();
  }

  Future<Comment?> createComs(Comment comment) async {
    String? postid = comment.postId.toString();
    var client = http.Client();
    var uri =
        Uri.parse('$url_post$postid/comments');
    final http.Response response = await client.post(
      uri,
      body: comment.toJson(),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1'
      },
    );
    if (response.statusCode == 200) {
      return Comment.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
    }
    return null;
  }

  Future<List<Post>?> getPosts() async {
    var client = http.Client();
    var uri1 = Uri.parse(url_post);
    var response1 = await client.get(
      uri1,
      headers: <String, String>{
        'Authorization':
            'Bearer $token1'
      },
    );
    if (response1.statusCode == 200) {
      var json = response1.body;
      return postFromJson(json);
    }
  }

  Future<Post?> createPosts(Post post, BuildContext context) async {
    String? userId = post.userId.toString();
    var uri = Uri.parse('$url_user$userId/posts');
    final http.Response response = await http.post(
      uri,
      body: post.toJson(),
      headers: <String, String>{
        'Authorization':
            'Bearer $token1'
      },
    );
    if (response.statusCode == 200) {
      print(response.body);
      return Post.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode);
      // check = 1;
      // showDialog(context: context , builder: (context) => AlertDialog(title: Text("thong bao"),content: Text("submit thanh cong"),actions: [
      //   TextButton(onPressed:
      //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (Buildercontext context) => commentPage(post1: post1, usermodel: usermodel),), (route) => false), child: Text("ok"))
      // ],));

    }
  }
}
