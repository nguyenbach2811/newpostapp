// ignore_for_file: prefer_const_constructors, camel_case_types, prefer_const_constructors_in_immutables, unnecessary_import, file_names, unused_field, non_constant_identifier_names, avoid_print

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/HomePage.dart';
import 'package:post/value/images.dart';
import 'package:post/value/styles.dart';
import 'package:post/widgets/CustomPageRoute.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/bottombar.dart';
import 'package:post/widgets/getTextFormField.dart';
import 'package:post/Model/PostModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class newPosts extends StatefulWidget {
  newPosts({required this.userModel, Key? key}) : super(key: key);
  UserModel userModel = new UserModel();

  @override
  State<newPosts> createState() => _newPostsState();
}

class _newPostsState extends State<newPosts> {
  final titleController = TextEditingController();
  final bodyController = TextEditingController();
  Future<SharedPreferences> pref = SharedPreferences.getInstance();
  final formkey = GlobalKey<FormState>();
  String? name;
  int? user_id;
  @override
  void initState() {
    user_id = widget.userModel.id;
    super.initState();
    // _conStatus.text;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formkey,
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage(AppImages.backGround),
                      fit: BoxFit.cover)),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(),
              ),
            ),
            Column(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 100,
                      ),
                      GetTextFormField(
                        controller: titleController,
                        color: Colors.grey.shade100.withOpacity(0.5),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GetTextFormField(
                        controller: bodyController,
                        color: Colors.grey.shade100.withOpacity(0.5),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      GestureDetector(
                        onTap: () async {
                          if (titleController.text.isEmpty ||
                              bodyController.text.isEmpty)
                            alertDialog('nhap day du tittle va body');
                          else {
                            Post newposts = Post(
                                userId: user_id,
                                title: titleController.text,
                                body: bodyController.text);
                            await ApiService()
                                .createPosts(newposts, context)
                                .then((data) {
                              if (data == null) {
                                alertDialog("Successful");
                                Future.delayed(Duration(seconds: 1)).then(
                                    (value) => Navigator.pushAndRemoveUntil(
                                        context,
                                        CustomPageRoute(
                                            child: BottomBar(
                                              userModel: widget.userModel,
                                            ),
                                            direction: AxisDirection.left),
                                        (route) => false));
                              }
                            });
                          }
                          ;
                        },
                        child: Container(
                          height: 50,
                          width: 200,
                          decoration: BoxDecoration(
                            gradient: const LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [
                                Color.fromARGB(255, 243, 247, 246),
                                Color.fromARGB(255, 239, 244, 244),
                              ],
                            ),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: Center(
                            child: Text('Upload',
                                style: AppStyles.h3.copyWith(
                                    fontSize: 25, color: Colors.black)),
                          ),
                        ),
                      ),
                      // ElevatedButton(
                      //     child: Text('Upload'),
                      //     onPressed: () async {
                      //       Post newposts = Post(
                      //           userId: user_id,
                      //           title: titleController.text,
                      //           body: bodyController.text);
                      //       await PostData().createPosts(newposts);
                      //     }),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
    // SingleChildScrollView(
    //   child: Padding(
    //     padding: EdgeInsets.all(8.0),
    //     child: Column(
    //       children: [
    //         Padding(
    //           padding: EdgeInsets.all(8.0),
    //           child: TextField(
    //             minLines: null,
    //             maxLines: null,
    //             decoration: InputDecoration(
    //               labelText: 'Nhap tieu de',
    //               fillColor: Colors.black12,
    //               filled: false,
    //               border: OutlineInputBorder(
    //                   borderRadius: BorderRadius.all(Radius.circular(8.0))),
    //             ),
    //             controller: titleController,
    //           ),
    //         ),
    //         Padding(
    //           padding: EdgeInsets.all(8.0),
    //           child: TextField(
    //             // minLines: null,
    //             // maxLines: null,
    //             decoration: InputDecoration(
    //               labelText: 'Nhap body',
    //               fillColor: Colors.black12,
    //               filled: false,
    //               border: OutlineInputBorder(
    //                   borderRadius: BorderRadius.all(Radius.circular(8.0))),
    //             ),
    //             controller: bodyController,
    //           ),
    //         ),
    //         ElevatedButton(
    //             child: Text('Upload'),
    //             onPressed: () async {
    //               Post newposts = Post(
    //                   userId: defuserId,
    //                   title: titleController.text,
    //                   body: bodyController.text);
    //               await PostData().createPosts(newposts);
    //             }),
    //       ],
    //     ),
    //   ),
    // ),
  }
}
