import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:post/Pages/LoginPage.dart';
import 'package:post/Pages/RegisterPage.dart';
import 'package:post/value/images.dart';
import 'package:post/value/styles.dart';

class StartedPage extends StatelessWidget {
  const StartedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage(AppImages.backGround),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 150, left: 50),
                child: Text(
                  "WELCOME TO MY APP",
                  style: AppStyles.h3.copyWith(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
              Container(
                width: 350,
                padding: const EdgeInsets.only(left: 50, top: 20),
                child: Text(
                  "Let's connect with people all over the world",
                  style: AppStyles.h3.copyWith(fontSize: 20),
                ),
              ),
            ],
          ),
          Column(
            children: [
              const SizedBox(
                width: 40,
              ),
              Container(
                padding: const EdgeInsets.only(top: 400),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginPage()),
                          (route) => false);
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('SIGN IN',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 40,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterPage()),
                          (route) => false);
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('SIGN UP',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 170),
                child: Center(
                  child: Text(
                    'Login with Social Media',
                    style: AppStyles.h3.copyWith(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 130),
                      child: IconButton(
                        icon: Icon(Icons.facebook),
                        color: Colors.white,
                        iconSize: 40,
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: Icon(FontAwesomeIcons.twitter),
                        color: Colors.white,
                        iconSize: 40,
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      child: IconButton(
                        icon: Icon(FontAwesomeIcons.instagram),
                        color: Colors.white,
                        iconSize: 40,
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
