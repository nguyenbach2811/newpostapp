// ignore_for_file: duplicate_import, avoid_print

import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/LoginPage.dart';
import 'package:post/value/images.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextFormField.dart';
import '../value/styles.dart';
import 'package:post/value/styles.dart';
import 'package:post/widgets/CustomPageRoute.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextField.dart';
import 'package:post/widgets/getTextFormField.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final formKey = GlobalKey<FormState>();
  final _conName = TextEditingController();
  final _conEmail = TextEditingController();
  int _gender = 0;
  int _status = 0;

  ConnectivityResult result = ConnectivityResult.none;

  signUp() async {
    String name = _conName.text;
    String email = _conEmail.text;
    late String gender;
    late String status;
    final bool isValidEmail = EmailValidator.validate(email);
    if (_gender == 1) {
      gender = 'male';
    } else if (_gender == 2) {
      gender = 'female';
    }

    if (_status == 3) {
      status = 'active';
    } else if (_status == 4) {
      status = 'inactive';
    }

    if (formKey.currentState!.validate()) {
      if (name.isEmpty) {
        alertDialog("Please input name!");
      } else if (email.isEmpty) {
        alertDialog("Please input email");
      } else if (isValidEmail == false) {
        alertDialog("Invalid email");
      } else if (_gender == 0) {
        alertDialog("please choose gender!");
      } else if (_status == 0) {
        alertDialog("please choose status!");
      } else {
        formKey.currentState!.save();
        UserModel user =
            UserModel(name: name, email: email, gender: gender, status: status);

        await ApiService().register(user).then((data) {
          if (data != null) {
            alertDialog("Successful");
            Navigator.pushAndRemoveUntil(
                context,
                CustomPageRoute(
                    child: const LoginPage(), direction: AxisDirection.left),
                (route) => false);
          } else {
            alertDialog("User Exist");
          }
        }).catchError((error) {
          print(error);
          alertDialog("Error");
        });
      }
    }
  }

  checkInternet() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.mobile) {
      signUp();
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: Container(
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('ok'),
                    ),
                  ),
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomCenter,
              colors: [
            Colors.purple,
            Colors.blue,
            Color.fromARGB(255, 53, 38, 163)
          ])),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        body: Form(
          key: formKey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: size.height * 1 / 4.5,
                ),
                Stack(
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: size.height * 1 / 25,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: size.height * 1 / 7,
                        ),
                        GetTextField(
                          controller: _conName,
                          icon: Icons.person,
                          color: Colors.transparent,
                        ),
                        GetTextField(
                          controller: _conEmail,
                          icon: Icons.email,
                          color: Colors.transparent,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text('Gender',
                            style: AppStyles.h3.copyWith(
                              fontSize: 20,
                              color: Colors.white,
                            )),
                        Row(
                          children: [
                            const SizedBox(
                              width: 30,
                            ),
                            SizedBox(
                              height: 40,
                              width: 150,
                              child: ListTile(
                                title: Text(
                                  'male',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 20, color: Colors.white),
                                ),
                                leading: Radio(
                                  fillColor:
                                      MaterialStateProperty.all(Colors.white),
                                  value: 1,
                                  groupValue: _gender,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _gender = int.parse(newValue.toString());
                                    });
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 50,
                            ),
                            SizedBox(
                              height: 40,
                              width: 150,
                              child: ListTile(
                                title: Text(
                                  'female',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 20, color: Colors.white),
                                ),
                                leading: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: Radio(
                                    fillColor:
                                        MaterialStateProperty.all(Colors.white),
                                    value: 2,
                                    groupValue: _gender,
                                    onChanged: (newValue) {
                                      setState(() {
                                        _gender =
                                            int.parse(newValue.toString());
                                        print(_gender);
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Status',
                          style: AppStyles.h3
                              .copyWith(fontSize: 20, color: Colors.white),
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 30,
                            ),
                            SizedBox(
                              height: 40,
                              width: 150,
                              child: ListTile(
                                title: Text(
                                  'active',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 20, color: Colors.white),
                                ),
                                leading: Radio(
                                  fillColor:
                                      MaterialStateProperty.all(Colors.white),
                                  value: 3,
                                  groupValue: _status,
                                  onChanged: (newValue1) {
                                    setState(() {
                                      _status = int.parse(newValue1.toString());
                                    });
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 50,
                            ),
                            SizedBox(
                              height: 40,
                              width: 180,
                              child: ListTile(
                                title: Text(
                                  'inactive',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 20, color: Colors.white),
                                ),
                                leading: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: Radio(
                                    fillColor:
                                        MaterialStateProperty.all(Colors.white),
                                    value: 4,
                                    groupValue: _status,
                                    onChanged: (newValue1) {
                                      setState(() {
                                        _status =
                                            int.parse(newValue1.toString());
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: size.height * 1 / 25,
                        ),
                        GestureDetector(
                          onTap: () {
                            signUp();
                          },
                          child: Container(
                            height: 50,
                            width: 200,
                            decoration: BoxDecoration(
                              gradient: const LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Color.fromARGB(255, 243, 247, 246),
                                  Color.fromARGB(255, 239, 244, 244),
                                ],
                              ),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Center(
                              child: Text('SIGN UP',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 25, color: Colors.white)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.height * 1 / 25,
                        ),
                        Container(
                          padding: const EdgeInsets.only(right: 40),
                          alignment: Alignment.bottomRight,
                          child: Text(
                            'Have account?',
                            style: AppStyles.h3
                                .copyWith(color: Colors.white, fontSize: 20),
                          ),
                        ),
                        Container(
                          alignment: Alignment.bottomRight,
                          padding: const EdgeInsets.only(right: 40),
                          child: TextButton(
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    CustomPageRoute(
                                        child: const LoginPage(),
                                        direction: AxisDirection.left),
                                    (route) => false);
                              },
                              child: Text(
                                "Sign in",
                                style: AppStyles.h3.copyWith(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w800,
                                    color: Color.fromARGB(255, 133, 130, 130)),
                              )),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
