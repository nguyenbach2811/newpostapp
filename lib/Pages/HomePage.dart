// ignore_for_file: file_names, unused_import, prefer_const_constructors, must_be_immutable, use_key_in_widget_constructors, sort_child_properties_last, non_constant_identifier_names, unused_local_variable
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/PostModel.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/CommentPage.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:post/value/images.dart';

class HomePage extends StatefulWidget {
  UserModel usermodel = new UserModel();
  HomePage({required this.usermodel});
  @override
  Demo createState() => Demo();
}

class Demo extends State<HomePage> {
  List<Post>? posts;
  var isLoaded = false;

  @override
  // void configLoading() {
  //   EasyLoading.instance
  //     ..displayDuration = const Duration(milliseconds: 2000)
  //     ..indicatorType = EasyLoadingIndicatorType.fadingCircle
  //     ..loadingStyle = EasyLoadingStyle.dark
  //     ..indicatorSize = 45.0
  //     ..radius = 10.0
  //     ..progressColor = Colors.yellow
  //     ..backgroundColor = Colors.green
  //     ..indicatorColor = Colors.yellow
  //     ..textColor = Colors.yellow
  //     ..maskColor = Colors.blue.withOpacity(0.5)
  //     ..userInteractions = true
  //     ..dismissOnTap = false;
  //   // ..customAnimation = CustomAnimation();
  // }

  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    posts = await ApiService().getPosts();
    setState(
      () {
        isLoaded = true;
      },
    );
  }

  final ApiService takeData = ApiService();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: build_AppBar(context),
      body: FutureBuilder(
        future: takeData.getPosts(),
        builder: (BuildContext context, AsyncSnapshot<List<Post>?> snapshot) {
          if (snapshot.hasData) {
            List<Post>? posts = snapshot.data;
            return Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage(AppImages.backGround),
                        fit: BoxFit.cover)),
                child: ListView(
                  children: posts!
                      .map(
                        (Post post) => Card(
                          margin: EdgeInsets.all(8.0),
                          child: ListTile(
                            // leading: CircleAvatar(
                            //     backgroundImage:
                            //         AssetImage("assets/images/avatar.png")),
                            title: Text(
                              post.title,
                              style: TextStyle(fontSize: 18),
                            ),
                            subtitle:
                                Text(post.body, style: TextStyle(fontSize: 14)),
                            onTap: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => commentPage(
                                post1: post,
                                usermodel: widget.usermodel,
                              ),
                            )),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

// void configLoading() {
//   EasyLoading.instance
//     ..displayDuration = const Duration(milliseconds: 2000)
//     ..indicatorType = EasyLoadingIndicatorType.fadingCircle
//     ..loadingStyle = EasyLoadingStyle.dark
//     ..indicatorSize = 45.0
//     ..radius = 10.0
//     ..progressColor = Colors.yellow
//     ..backgroundColor = Colors.green
//     ..indicatorColor = Colors.yellow
//     ..textColor = Colors.yellow
//     ..maskColor = Colors.blue.withOpacity(0.5)
//     ..userInteractions = true
//     ..dismissOnTap = false;
//   // ..customAnimation = CustomAnimation();
// }
