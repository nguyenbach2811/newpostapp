// ignore_for_file: camel_case_types

import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/CommentModel.dart';
import 'package:post/Model/PostModel.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/value/images.dart';
import 'package:post/value/styles.dart';
import 'package:post/widgets/CustomPageRoute.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextFormField.dart';
import 'HomePage.dart';
import 'CommentPage.dart';

class createComs extends StatefulWidget {
  int? postId;
  UserModel userModel = new UserModel();
  Post post1;
  // createComs(int? postId, {Key? key}) : super(key: key) {
  //   this.postId = postId;
  // }

  createComs(this.userModel, this.post1, {Key? key}) : super(key: key);
  @override
  State<createComs> createState() => _createComsState();
}

class _createComsState extends State<createComs> {
  ConnectivityResult result = ConnectivityResult.none;
  final comController = TextEditingController();
  final formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('new coms')),
      body: Form(
        key: formkey,
        child: Stack(children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(AppImages.backGround),
                    fit: BoxFit.cover)),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(),
            ),
          ),
          Column(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    GetTextFormField(
                      controller: comController,
                      color: Colors.grey.shade100.withOpacity(0.5),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (comController.text.isEmpty)
                          alertDialog('nhap cmt');
                        else {
                          Comment newcom = Comment(
                            postId: widget.post1.id,
                            name: widget.userModel.name!,
                            body: comController.text,
                            email: widget.userModel.email!,
                          );
                          await ApiService().createComs(newcom).then((data) {
                            // print(data);
                            if (data == null) {
                              // print(data);
                              alertDialog("Successful");
                              Future.delayed(Duration(seconds: 1)).then(
                                  (value) => Navigator.pushAndRemoveUntil(
                                      context,
                                      CustomPageRoute(
                                          child: commentPage(
                                              post1: widget.post1,
                                              usermodel: widget.userModel),
                                          direction: AxisDirection.left),
                                      (route) => false));
                            }
                            ;
                          }

                              // if (check == 1) {
                              //   showDialog(
                              //       context: context,
                              //       builder: (context) => AlertDialog(
                              //             title: Text("thong bao"),
                              //             content: Text("upload thanh cong"),
                              //             actions: [
                              //               TextButton(
                              //                   onPressed: (() {
                              //                     Navigator.pop(
                              //                         context,
                              //                         MaterialPageRoute(
                              //                           builder: (context) =>
                              //                               HomePage(
                              //                                   usermodel: widget
                              //                                       .userModel),
                              //                         ));
                              //                   }),
                              //                   child: Text("ok"))
                              //             ],
                              //           ));
                              // }
                              );
                        }
                        ;
                        // check = 0;
                      },
                      child: Container(
                        height: 50,
                        width: 200,
                        decoration: BoxDecoration(
                          gradient: const LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color.fromARGB(255, 243, 247, 246),
                              Color.fromARGB(255, 239, 244, 244),
                            ],
                          ),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Center(
                          child: Text('Upload',
                              style: AppStyles.h3
                                  .copyWith(fontSize: 25, color: Colors.black)),
                        ),
                      ),
                    ),
                    // ElevatedButton(
                    //     child: Text('Upload'),
                    //     onPressed: () async {
                    //       Post newposts = Post(
                    //           userId: user_id,
                    //           title: titleController.text,
                    //           body: bodyController.text);
                    //       await PostData().createPosts(newposts);
                    //     }),
                  ],
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }

//   check() async {
//     result = await Connectivity().checkConnectivity();
//     if (result == ConnectivityResult.mobile ||
//         result == ConnectivityResult.wifi) {
//       checkva();
//     } else if (result == ConnectivityResult.none) {
//       return showDialog(
//           context: context,
//           barrierDismissible: false,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: const Text('Please check internet connection'),
//               actions: <Widget>[
//                 Center(
//                   child: TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: const Text('Ok'),
//                   ),
//                 ),
//               ],
//             );
//           });
//     }
//   }
// }
}
