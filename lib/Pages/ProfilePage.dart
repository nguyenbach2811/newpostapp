import 'package:connectivity_plus/connectivity_plus.dart';
// ignore_for_file: unused_import

import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/HomePage.dart';
import 'package:post/Pages/StartedPage.dart';
import 'package:post/value/images.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextFormField.dart';

import '../value/styles.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key, required this.user3}) : super(key: key);
  UserModel? user3;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final _conEmail = TextEditingController();
  final _conName = TextEditingController();
  final _conGender = TextEditingController();
  final _conStatus = TextEditingController();
  final formkey = GlobalKey<FormState>();

  // bool hasInternet = false;
  ConnectivityResult result = ConnectivityResult.none;

  @override
  void initState() {
    super.initState();
    getData();
  }

  update() async {
    String email = _conEmail.text;
    String name = _conName.text;
    String gender1 = _conGender.text;
    String status1 = _conStatus.text;
    if (formkey.currentState!.validate()) {
      if (gender1 != 'male' && gender1 != 'female') {
        alertDialog('wrong gender!');
      } else if (status1 != 'active' && status1 != 'inactive') {
        alertDialog("Wrong status!");
      } else {
        formkey.currentState!.save();
        UserModel user1 = UserModel(
            email: email, name: name, gender: gender1, status: status1);

        await ApiService()
            .updateUser(widget.user3!.id.toString(), user1)
            .then((data) {
          setState(() {
            alertDialog("Successful Update!");
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => HomePage(
                          usermodel: user1,
                        )),
                (route) => false);
          });
        }).catchError((error) {
          print(error);
          alertDialog("Error");
        });
      }
    }
  }

  getData() {
    setState(() {
      _conName.text = widget.user3!.name!;
      _conEmail.text = widget.user3!.email!;
      _conGender.text = widget.user3!.gender!;
      _conStatus.text = widget.user3!.status!;
    });
  }

  check() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      update();
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  delete() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        ApiService().deleteUser(widget.user3!.id.toString());
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(usermodel: widget.user3!)),
            (route) => false);
      });
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  logOut() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      usermodel: widget.user3!,
                    )),
            (route) => false);
      });
    } else {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(color: Color.fromARGB(255, 29, 29, 29)),
      child: Scaffold(
        // extendBody: true,
        backgroundColor: Colors.transparent,
        body: Form(
            key: formkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: size.height * 1 / 10,
                  ),
                  GetTextFormField(
                    controller: _conName,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conEmail,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conGender,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  GetTextFormField(
                    controller: _conStatus,
                    color: Colors.grey.shade100.withOpacity(0.5),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () async {
                      check();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Update',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 1 / 25,
                  ),
                  GestureDetector(
                    onTap: () {
                      delete();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Delete',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.height * 1 / 25,
                  ),
                  GestureDetector(
                    onTap: () {
                      logOut();
                    },
                    child: Container(
                      height: 50,
                      width: 200,
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color.fromARGB(255, 243, 247, 246),
                            Color.fromARGB(255, 239, 244, 244),
                          ],
                        ),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                        child: Text('Log out',
                            style: AppStyles.h3
                                .copyWith(fontSize: 25, color: Colors.black)),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
