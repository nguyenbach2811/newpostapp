// ignore_for_file: camel_case_types, use_key_in_widget_constructors, sort_child_properties_last, prefer_const_constructors, file_names, unused_import

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/CommentModel.dart';
import 'package:post/Model/PostModel.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/NewComPage.dart';
import 'package:post/value/styles.dart';
import 'package:post/widgets/CustomPageRoute.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextField.dart';
import 'package:post/widgets/getTextFormField.dart';

class commentPage extends StatefulWidget {
  final Post post1;
  bool? check;
  UserModel usermodel = new UserModel();
  commentPage({required this.post1, required this.usermodel});
  @override
  State<commentPage> createState() => _commentPageState();
}

class _commentPageState extends State<commentPage> {
  List<Comment>? comments;
  var isload2 = false;
  final comController = TextEditingController();
  @override
  void initState() {
    super.initState();
    getData2();
  }

  getData2() async {
    comments = await ApiService().getCom(widget.post1.id.toString());
    if (comments != null) {
      isload2 = true;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(isload2.toString());
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.post1.title.toString()),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Visibility(
              visible: isload2,
              child: SizedBox(
                height: 500,
                width: 400,
                child: ListView.builder(
                  itemCount: comments?.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.grey[300]),
                          ),
                          SizedBox(width: 16),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  comments![index].email,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  comments![index].body,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
            GetTextFormField(
              controller: comController,
            ),
            InkWell(
                child: Icon(Icons.arrow_right_alt),
                onTap: () async {
                  if (comController.text.isEmpty)
                    alertDialog('nhap cmt');
                  else {
                    Comment newcom = Comment(
                      postId: widget.post1.id,
                      name: widget.usermodel.name!,
                      body: comController.text,
                      email: widget.usermodel.email!,
                    );
                    await ApiService().createComs(newcom).then((data) {
                      // print(data);
                      if (data == null) {
                        // print(data);
                        alertDialog("Successful");
                        comments?.insert(0, newcom);
                        setState(() {});
                        // getData2();
                        comController.text = "";
                      }
                      ;
                    });
                  }
                }),
            // GestureDetector(onTap: () async {
            //   if (comController.text.isEmpty)
            //     alertDialog('nhap cmt');
            //   else {
            //     Comment newcom = Comment(
            //       postId: widget.post1.id,
            //       name: widget.usermodel.name!,
            //       body: comController.text,
            //       email: widget.usermodel.email!,
            //     );
            //     await ApiService().createComs(newcom).then((data) {
            //       // print(data);
            //       if (data == null) {
            //         // print(data);
            //         alertDialog("Successful");
            //         Future.delayed(Duration(seconds: 1)).then((value) =>
            //             Navigator.pushAndRemoveUntil(
            //                 context,
            //                 CustomPageRoute(
            //                     child: commentPage(
            //                         post1: widget.post1,
            //                         usermodel: widget.usermodel),
            //                     direction: AxisDirection.left),
            //                 (route) => false));
            //       }
            //       ;
            //     });
            //   }
            // })
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.add),
      //   onPressed: () {
      //     Navigator.of(context).push(MaterialPageRoute(
      //       builder: (context) => createComs(widget.usermodel, widget.post1),
      //     ));
      //   },
      // ),
    );
  }
}
