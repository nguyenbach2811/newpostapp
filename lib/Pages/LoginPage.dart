import 'dart:ui';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:post/Api/Api_Service.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/RegisterPage.dart';
import 'package:post/value/images.dart';
import 'package:post/value/styles.dart';
import 'package:post/widgets/CustomPageRoute.dart';
import 'package:post/widgets/alert.dart';
import 'package:post/widgets/getTextField.dart';

import '../widgets/bottombar.dart';
import '../widgets/getTextFormField.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _conUserName = TextEditingController();
  final _conEmail = TextEditingController();
  final formkey = GlobalKey<FormState>();
  List<UserModel> users = [];

  ConnectivityResult result = ConnectivityResult.none;

  bool isApiCallprocess = false;
  @override
  void initState() {
    super.initState();
  }

  login() {
    String email1 = _conEmail.text;
    String name1 = _conUserName.text;
    bool check = false;
    final bool isValidEmail = EmailValidator.validate(email1);
    if (formkey.currentState!.validate()) {
      if (name1.isEmpty) {
        alertDialog("please input email!");
      } else if (email1.isEmpty) {
        alertDialog("please input name");
      } else if (isValidEmail == false) {
        alertDialog("Invalid Email");
      } else {
        formkey.currentState!.save();
        for (int i = 0; i < users.length; i++) {
          print(users[i].toJson());
          if (users[i].email!.contains(email1) &&
              users[i].name!.contains(name1)) {
            check = true;
            alertDialog("Complete");
            Navigator.pushAndRemoveUntil(
                context,
                CustomPageRoute(
                    child: BottomBar(
                      userModel: users[i],
                    ),
                    direction: AxisDirection.right),
                (route) => false);
          }
        }
        if (check == false) {
          alertDialog("User not found!!");
        }
      }
    }
  }

  check() async {
    result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      login();
    } else if (result == ConnectivityResult.none) {
      return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Please check internet connection'),
              actions: <Widget>[
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Ok'),
                  ),
                ),
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        reverse: true,
        child: Form(
          key: formkey,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: ExactAssetImage(AppImages.backGround),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 200,
                    ),
                    Stack(
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              height: 100,
                              width: 200,
                              child: FutureBuilder(
                                  future: ApiService().getAllUser(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<List<UserModel>> snapshot) {
                                    if (snapshot.hasData) {
                                      users = snapshot.data!;
                                      return Container();
                                    }
                                    return Center(
                                        child: Column(
                                      children: [
                                        const CircularProgressIndicator(),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Text(
                                          'Loading data .........',
                                          style: AppStyles.h3.copyWith(
                                              color: Colors.black,
                                              fontSize: 20),
                                        )
                                      ],
                                    ));
                                  }),
                            ),
                            GetTextFormField(
                              controller: _conUserName,
                              icon: Icons.person,
                              color: Colors.grey.shade100.withOpacity(0.5),
                            ),
                            GetTextFormField(
                              controller: _conEmail,
                              icon: Icons.email,
                              color: Colors.grey.shade100.withOpacity(0.5),
                            ),
                            const SizedBox(
                              height: 50,
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isApiCallprocess = true;
                                });
                                login();
                              },
                              child: Container(
                                height: 50,
                                width: 200,
                                decoration: BoxDecoration(
                                  gradient: const LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: [
                                      Color.fromARGB(255, 243, 247, 246),
                                      Color.fromARGB(255, 239, 244, 244),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Center(
                                  child: Text('SIGN IN',
                                      style: AppStyles.h3.copyWith(
                                          fontSize: 25, color: Colors.black)),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 50,
                            ),
                            Center(
                              child: Container(
                                child: TextButton(
                                    onPressed: () {
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const RegisterPage()),
                                          (route) => false);
                                    },
                                    child: Text("Create your account",
                                        style: AppStyles.h3.copyWith(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                        ))),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
