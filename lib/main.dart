
import 'package:app1/Model/UserModel.dart';
import 'package:app1/View/Pages/HomePage.dart';
import 'package:app1/View/Pages/LoadingPage.dart';
import 'package:app1/View/Pages/StartedPage.dart';
import 'package:app1/View/widgets/bottombar.dart';
import 'package:flutter/material.dart';
import 'package:app1/Model/UserModel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


void main() {
  runApp(
       MyApp(),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final storage = new FlutterSecureStorage();
  Widget page = LoadingPage();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkLogin();
  }

  void checkLogin() async {
    String? token = await storage.read(key: 'token');
    print(token);
    if (token != null){
      setState(() {
        page = BottomBar();
      });
    }else {
      setState(() {
        page = StartedPage();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: page,
    );
  }
}

