import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:post/value/fonts.dart';

class AppStyles {
  static const TextStyle h3 = TextStyle(
    fontSize: 30,
    fontFamily: AppFonts.sfd,
    color: Colors.white,
  );
}
