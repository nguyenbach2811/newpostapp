import 'package:flutter/material.dart';
import 'package:post/value/styles.dart';
import 'package:toast/toast.dart';

class GetTextField extends StatelessWidget {
  TextEditingController? controller;
  String? hintName;
  IconData? icon;
  bool isObscureText = false;
  TextInputType inputType;
  bool _passwordVisible = false;
  Color? color;

  GetTextField(
      {this.controller,
      this.hintName,
      this.icon,
      this.color,
      this.inputType = TextInputType.text,
      this.isObscureText = false});
  @override
  void initState() {
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Container(
      padding: const EdgeInsets.only(top: 25, left: 50, right: 50),
      child: TextField(
        controller: controller,
        obscureText: isObscureText,
        keyboardType: inputType,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Color.fromARGB(255, 215, 207, 207))),
            prefixIcon: Icon(icon, color: Color.fromARGB(255, 215, 207, 207)),
            hintText: hintName,
            fillColor: color,
            filled: true,
            hintStyle:
                AppStyles.h3.copyWith(fontSize: 15, color: Colors.white)),
      ),
    );
  }
}
