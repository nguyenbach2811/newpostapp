import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class GetTextFormField extends StatelessWidget {
  TextEditingController? controller;
  String? hintName;
  String? labelName;
  IconData? icon;
  bool isObscureText = false;
  TextInputType inputType;
  bool _passwordVisible = false;
  Color? color;

  GetTextFormField(
      {this.controller,
      this.hintName,
      this.labelName,
      this.icon,
      this.color,
      this.inputType = TextInputType.text,
      this.isObscureText = false});
  @override
  void initState() {
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Container(
      padding: const EdgeInsets.only(top: 25, left: 50, right: 50),
      child: TextFormField(
        controller: controller,
        obscureText: isObscureText,
        keyboardType: inputType,
        textAlign: TextAlign.center,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter $hintName';
          }
          return null;
        },
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.transparent)),
            focusedBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.white)),
            errorStyle: TextStyle(color: Colors.red),
            focusedErrorBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.white)),
            errorBorder: UnderlineInputBorder(
                borderRadius: BorderRadius.circular(40),
                borderSide: BorderSide(color: Colors.red)),
            prefixIcon: Icon(icon),
            labelText: labelName,
            hintText: hintName,
            contentPadding: const EdgeInsets.only(left: 20),
            fillColor: color,
            filled: true),
      ),
    );
  }
}
