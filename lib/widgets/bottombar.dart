// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'dart:ui';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:post/Model/UserModel.dart';
import 'package:post/Pages/HomePage.dart';
import 'package:post/Pages/ProfilePage.dart';
import '../Pages/StartedPage.dart';
import '../Pages/NewPostsPage.dart';

class BottomBar extends StatefulWidget {
  const BottomBar({required this.userModel, Key? key}) : super(key: key);
  final UserModel userModel;
  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int currentIndex = 0;
  int? tappedIndex;
  late UserModel user;
  late var Screens;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = widget.userModel;
    Screens = [
      HomePage(
        usermodel: user,
      ),
      newPosts(
        userModel: user,
      ),
      ProfilePage(user3: user),
    ];
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBody: true,
      backgroundColor: Color.fromARGB(255, 6, 35, 59),
      body: PageTransitionSwitcher(
        transitionBuilder: (child, primaryAnimation, secondaryAnimation) =>
            FadeThroughTransition(
          animation: primaryAnimation,
          secondaryAnimation: secondaryAnimation,
          child: child,
        ),
        child: Screens[currentIndex],
      ),
      bottomNavigationBar: Container(
        child: Theme(
          data: Theme.of(context).copyWith(
            canvasColor:
                const Color.fromARGB(255, 207, 209, 216).withOpacity(0.8),
          ),
          child: Container(
            height: size.height * 1 / 11,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16), topRight: Radius.circular(16)),
              child: ClipRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 5,
                    sigmaY: 10,
                  ),
                  child: BottomNavigationBar(
                    type: BottomNavigationBarType.fixed,
                    selectedItemColor: const Color.fromARGB(255, 47, 100, 181),
                    // iconSize: 30,
                    // selectedFontSize: 18,
                    // unselectedFontSize: 15,
                    items: const <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.home),
                        label: 'Home',
                      ),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.add), label: 'Add'),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.person), label: 'Profile'),
                    ],
                    currentIndex: currentIndex,
                    onTap: (index) {
                      setState(() {
                        currentIndex = index;
                      });
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
